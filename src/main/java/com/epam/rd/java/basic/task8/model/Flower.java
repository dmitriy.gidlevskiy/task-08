package com.epam.rd.java.basic.task8.model;

public class Flower {
    String name;
    String soil;
    String origin;
    String stemColour;
    String leafColour;
    String aveLenFlowerMeasureAttr;
    int aveLenFlowerUnit;
    String tempretureMeasureAttr;
    int tempretureUnit;
    String lightRequiring;
    String wateringMeasureAttr;
    int wateringUnit;
    String multiplying;

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlowerMeasureAttr='" + aveLenFlowerMeasureAttr + '\'' +
                ", aveLenFlowerUnit=" + aveLenFlowerUnit +
                ", tempretureMeasureAttr='" + tempretureMeasureAttr + '\'' +
                ", tempretureUnit=" + tempretureUnit +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", wateringMeasureAttr='" + wateringMeasureAttr + '\'' +
                ", wateringUnit=" + wateringUnit +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public String getAveLenFlowerMeasureAttr() {
        return aveLenFlowerMeasureAttr;
    }

    public void setAveLenFlowerMeasureAttr(String aveLenFlowerMeasureAttr) {
        this.aveLenFlowerMeasureAttr = aveLenFlowerMeasureAttr;
    }

    public int getAveLenFlowerUnit() {
        return aveLenFlowerUnit;
    }

    public void setAveLenFlowerUnit(int aveLenFlowerUnit) {
        this.aveLenFlowerUnit = aveLenFlowerUnit;
    }

    public String getTempretureMeasureAttr() {
        return tempretureMeasureAttr;
    }

    public void setTempretureMeasureAttr(String tempretureMeasureAttr) {
        this.tempretureMeasureAttr = tempretureMeasureAttr;
    }

    public int getTempretureUnit() {
        return tempretureUnit;
    }

    public void setTempretureUnit(int tempretureUnit) {
        this.tempretureUnit = tempretureUnit;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getWateringMeasureAttr() {
        return wateringMeasureAttr;
    }

    public void setWateringMeasureAttr(String wateringMeasureAttr) {
        this.wateringMeasureAttr = wateringMeasureAttr;
    }

    public int getWateringUnit() {
        return wateringUnit;
    }

    public void setWateringUnit(int wateringUnit) {
        this.wateringUnit = wateringUnit;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}