package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flower;
import org.w3c.dom.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getListFromXml() {
        List<Flower> flowers = new ArrayList<>();
        Flower flower;

        Schema schema = getXSDSchema("input.xsd");
        Document document = getDocumentFromXMLFile(xmlFileName);
        document.getDocumentElement().normalize();

        try {
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlFileName)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList flowerNodes = document.getElementsByTagName("flower");
        for (int temp = 0; temp < flowerNodes.getLength(); temp++) {
            Node node = flowerNodes.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;

                flower = new Flower();

                flower.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                flower.setSoil(eElement.getElementsByTagName("soil").item(0).getTextContent());
                flower.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
                flower.setStemColour(eElement.getElementsByTagName("stemColour").item(0).getTextContent());
                flower.setLeafColour(eElement.getElementsByTagName("leafColour").item(0).getTextContent());
                flower.setAveLenFlowerMeasureAttr(eElement.getElementsByTagName("aveLenFlower").item(0).getAttributes().getNamedItem("measure").getTextContent());
                flower.setAveLenFlowerUnit(Integer.parseInt(eElement.getElementsByTagName("aveLenFlower").item(0).getTextContent()));
                flower.setTempretureMeasureAttr(eElement.getElementsByTagName("tempreture").item(0).getAttributes().getNamedItem("measure").getTextContent());
                flower.setTempretureUnit(Integer.parseInt(eElement.getElementsByTagName("tempreture").item(0).getTextContent()));
                flower.setLightRequiring(eElement.getElementsByTagName("lighting").item(0).getAttributes().getNamedItem("lightRequiring").getTextContent());
                flower.setWateringMeasureAttr(eElement.getElementsByTagName("watering").item(0).getAttributes().getNamedItem("measure").getTextContent());
                flower.setWateringUnit(Integer.parseInt(eElement.getElementsByTagName("watering").item(0).getTextContent()));
                flower.setMultiplying(eElement.getElementsByTagName("multiplying").item(0).getTextContent());

                flowers.add(flower);
            }
        }
        return flowers;
    }



    public static Schema getXSDSchema(String schemaFileName) {
        Schema schema = null;
        try {
            String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
            SchemaFactory factory = SchemaFactory.newInstance(language);
            schema = factory.newSchema(new File(schemaFileName));
        } catch (Exception e) {
            System.out.println(e);
        }
        return schema;
    }

    public static Document getDocumentFromXMLFile(String fileName) {
        Document document = null;
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
            document = builder.parse(new File(fileName));
        } catch (Exception e) {
            System.out.println(e);
        }
        return document;
    }


    public void saveListToXMLFile(List<Flower> flowers, String xmlOutput) {
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            documentFactory.setNamespaceAware(true);

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element flowersRoot = document.createElementNS("http://www.nure.ua", "flowers");
            document.appendChild(flowersRoot);

            for (Flower fl : flowers) {
                Element flower = document.createElement("flower");
                flowersRoot.appendChild(flower);

                Element name = document.createElement("name");
                name.appendChild(document.createTextNode(fl.getName()));
                flower.appendChild(name);

                Element soil = document.createElement("soil");
                soil.appendChild(document.createTextNode(fl.getSoil()));
                flower.appendChild(soil);

                Element origin = document.createElement("origin");
                origin.appendChild(document.createTextNode(fl.getOrigin()));
                flower.appendChild(origin);

                Element visualParameters = document.createElement("visualParameters");
                flower.appendChild(visualParameters);

                Element stemColour = document.createElement("stemColour");
                stemColour.appendChild(document.createTextNode(fl.getStemColour()));
                visualParameters.appendChild(stemColour);

                Element leafColour = document.createElement("leafColour");
                leafColour.appendChild(document.createTextNode(fl.getLeafColour()));
                visualParameters.appendChild(leafColour);

                Element aveLenFlower = document.createElement("aveLenFlower");
                Attr measureLen = document.createAttribute("measure");
                measureLen.setValue(fl.getAveLenFlowerMeasureAttr());
                aveLenFlower.setAttributeNode(measureLen);
                aveLenFlower.appendChild(document.createTextNode(String.valueOf(fl.getAveLenFlowerUnit())));
                visualParameters.appendChild(aveLenFlower);


                Element growingTips = document.createElement("growingTips");
                flower.appendChild(growingTips);

                Element tempreture = document.createElement("tempreture");
                Attr measureTemp = document.createAttribute("measure");
                measureTemp.setValue(fl.getTempretureMeasureAttr());
                tempreture.setAttributeNode(measureTemp);
                tempreture.appendChild(document.createTextNode(String.valueOf(fl.getTempretureUnit())));
                growingTips.appendChild(tempreture);

                Element lighting = document.createElement("lighting");
                Attr lightRequiring = document.createAttribute("lightRequiring");
                lightRequiring.setValue(fl.getLightRequiring());
                lighting.setAttributeNode(lightRequiring);
                growingTips.appendChild(lighting);

                Element watering = document.createElement("watering");
                Attr measureWat = document.createAttribute("measure");
                measureWat.setValue(fl.getWateringMeasureAttr());
                watering.setAttributeNode(measureWat);
                watering.appendChild(document.createTextNode(String.valueOf(fl.getWateringUnit())));
                growingTips.appendChild(watering);

                Element multiplying = document.createElement("multiplying");
                multiplying.appendChild(document.createTextNode(fl.getMultiplying()));
                flower.appendChild(multiplying);

            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            Source domSource = new DOMSource(document);
            Result streamResult = new StreamResult(new FileWriter(xmlOutput));

            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException | IOException | TransformerException pce) {
            pce.printStackTrace();
        }

    }
    public static void main(String[] args) {
        DOMController domController = new DOMController("input.xml");
        List<Flower> flowers = domController.getListFromXml();
        String output = "output.xml";
        domController.saveListToXMLFile(flowers, output);
        System.out.println(output);

    }
}
