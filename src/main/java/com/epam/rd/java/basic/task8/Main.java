package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;

import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args)  {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
//		String xmlFileName = "input.xml";
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> domExtractedList = domController.getListFromXml();
		System.out.println("DOM " + domExtractedList);
		domExtractedList.sort(Comparator.comparing(Flower::getName));

		// PLACE YOUR CODE HERE

		// sort (case 1)
		// PLACE YOUR CODE HERE
//		public static void main(String[] args) {
//			DOMController domController = new DOMController("input.xml");
//			List<Flower> flowers = domController.getListFromXml();
//			String output = "output.xml";
//			domController.saveListToXMLFile(flowers, output);
//			System.out.println(output);
//
//		}
		// save
		String outputXmlFile = "output.dom.xml";
		domController.saveListToXMLFile(domExtractedList, outputXmlFile);
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> saxExtractedList = saxController.getListFromXml();
		System.out.println("SAX " + saxExtractedList);

		saxExtractedList.sort(Comparator.comparing(Flower::getSoil));

		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		domController.saveListToXMLFile(saxExtractedList, outputXmlFile);

		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> staxExtractedList = staxController.getListFromXml();
		System.out.println("STAX " + staxExtractedList);

		staxExtractedList.sort(Comparator.comparing(Flower::getLeafColour));


		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		domController.saveListToXMLFile(staxExtractedList, outputXmlFile);

		// PLACE YOUR CODE HERE
	}

}
