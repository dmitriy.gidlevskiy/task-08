package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getListFromXml() {


        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setNamespaceAware(true);
        try {
            saxParserFactory.setFeature(FEATURE_TURN_VALIDATION_ON, true);
            saxParserFactory.setFeature(FEATURE_TURN_SCHEMA_VALIDATION_ON, true);

        } catch (ParserConfigurationException | SAXNotRecognizedException | SAXNotSupportedException e) {
            e.printStackTrace();
        }


        SAXParser saxParser;
        try {
            saxParser = saxParserFactory.newSAXParser();

            saxParser.parse(new File(xmlFileName), this);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            return null;
        }
        return flowers;
    }

    private String currentTagName;
    List<Flower> flowers = new ArrayList<>();
    Flower flower = new Flower();



    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentTagName = localName;
        String attributeName = attributes.getQName(0);
        String attributeValue = attributes.getValue(0);
        if (attributeName != null) {
            if (attributeName.equals("measure")) {
                switch (currentTagName) {
                    case "aveLenFlower":
                        flower.setAveLenFlowerMeasureAttr(attributeValue);
                        break;
                    case "tempreture":
                        flower.setTempretureMeasureAttr(attributeValue);
                        break;
                    case "watering":
                        flower.setWateringMeasureAttr(attributeValue);
                        break;
                }
            } else if (attributeName.equals("lightRequiring")) flower.setLightRequiring(attributeValue);

        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName == null) return;

        if (qName.equals("flower")) {
            flowers.add(flower);
            flower = new Flower();
        }

        currentTagName = null;
    }


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentTagName == null) return;
        String elemText = new String(ch, start, length).trim();
        if (elemText.isEmpty()) return;

        if ("name".equals(currentTagName)) {
            flower.setName(elemText);
            return;
        }
        if ("soil".equals(currentTagName)) {
            flower.setSoil(elemText);
            return;
        }
        if ("origin".equals(currentTagName)) {
            flower.setOrigin(elemText);
            return;
        }
        if ("stemColour".equals(currentTagName)) {
            flower.setStemColour(elemText);
            return;
        }
        if ("leafColour".equals(currentTagName)) {
            flower.setLeafColour(elemText);
            return;
        }
        if ("aveLenFlower".equals(currentTagName)) {
            flower.setAveLenFlowerUnit(Integer.parseInt(elemText));
            return;
        }
        if ("tempreture".equals(currentTagName)) {
            flower.setTempretureUnit(Integer.parseInt(elemText));
            return;
        }
        if ("watering".equals(currentTagName)) {
            flower.setWateringUnit(Integer.parseInt(elemText));
            return;
        }
        if ("multiplying".equals(currentTagName)) {
            flower.setMultiplying(elemText);
            return;
        }


    }

    public static void main(String[] args) {
        SAXController saxController = new SAXController("input.xml");
        List<Flower> flowerList = saxController.getListFromXml();
        System.out.println("FlowerList: " + flowerList);
        System.out.println(flowerList.size());
    }

    // PLACE YOUR CODE HERE

}