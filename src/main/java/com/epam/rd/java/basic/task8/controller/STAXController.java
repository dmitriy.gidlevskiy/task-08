package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getListFromXml()   {
        List<Flower> flowers = new ArrayList<>();
        Flower flower = new Flower();


        String currElem = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

        XMLEventReader xmlEventReader = null;
        try {
            xmlEventReader = xmlInputFactory.createXMLEventReader(new StreamSource(xmlFileName));
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = null;
            try {
                xmlEvent = xmlEventReader.nextEvent();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            if (xmlEvent.isCharacters() && xmlEvent.asCharacters().isWhiteSpace()) continue;

            // ATTRIBUTES
            if (xmlEvent.isStartElement()) {
                StartElement startElem = xmlEvent.asStartElement();
                currElem = startElem.getName().getLocalPart();

                if(currElem.equals("aveLenFlower")) {
                    Attribute aveAttr = startElem.getAttributeByName(new QName("measure"));
                    flower.setAveLenFlowerMeasureAttr(aveAttr.getValue());
                }
                if(currElem.equals("tempreture")) {
                    Attribute aveAttr = startElem.getAttributeByName(new QName("measure"));
                    flower.setTempretureMeasureAttr(aveAttr.getValue());
                }
                if(currElem.equals("lighting")) {
                    Attribute aveAttr = startElem.getAttributeByName(new QName("lightRequiring"));
                    flower.setLightRequiring(aveAttr.getValue());
                }
                if(currElem.equals("watering")) {
                    Attribute aveAttr = startElem.getAttributeByName(new QName("measure"));
                    flower.setWateringMeasureAttr(aveAttr.getValue());
                }
            }

            /// CONTENT
                if (xmlEvent.isCharacters()) {
                    Characters characters = xmlEvent.asCharacters();


                if ("name".equals(currElem)) {
                    flower.setName(characters.getData());
                    continue;
                }
                if ("soil".equals(currElem)) {
                    flower.setSoil(characters.getData());
                    continue;
                }
                if ("origin".equals(currElem)) {
                    flower.setOrigin(characters.getData());
                    continue;
                }
                if ("stemColour".equals(currElem)) {
                    flower.setStemColour(characters.getData());
                    continue;
                }
                if ("leafColour".equals(currElem)) {
                    flower.setLeafColour(characters.getData());
                    continue;
                }
                if ("aveLenFlower".equals(currElem)) {
                    flower.setAveLenFlowerUnit(Integer.parseInt(characters.getData()));
                    continue;
                }
                if ("tempreture".equals(currElem)) {
                    flower.setTempretureUnit(Integer.parseInt(characters.getData()));
                    continue;
                }
                if ("watering".equals(currElem)) {
                    flower.setWateringUnit(Integer.parseInt(characters.getData()));
                    continue;
                }
                if ("multiplying".equals(currElem)) {
                    flower.setMultiplying(characters.getData());
                    continue;
                }
            }

            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                String localName = endElement.getName().getLocalPart();
                if("flower".equals(localName)) {
                    flowers.add(flower);
                    flower = new Flower();
                }


            }
        }
        try {
            xmlEventReader.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return flowers;
        }



    public static void main(String[] args) throws XMLStreamException {
        STAXController staxController = new STAXController("input.xml");
       List<Flower> flowers = staxController.getListFromXml();
    }

}